export class Game {
    public ScoreNous : number;
    public ScoreEux: number;

    constructor(scoreNous: number, scoreEux: number) {
        this.ScoreNous = scoreNous;
        this.ScoreEux = scoreEux;
    }
}