import { Component, ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
    selector: 'page-add',
    templateUrl: 'add.html'
})
export class AddPage {
    @ViewChild('selBelote') selbelote;

    private contratNous: string;
    private contratEux: string;
    private pointsNous: number;
    private pointsEux: number;
    private totalNous: number;
    private totalEux: number;
    private type: string;
    private beloteNous: string;
    private beloteEux: string;
    private coincher: boolean;
    private surcoincher: boolean;

    constructor(public navCtrl: NavController) {
        this.type = '162';
        this.beloteNous = '0';
        this.beloteEux = '0';
        this.contratEux = '0';
        this.contratNous = '0';
    }

    nousChanged() {
        this.total(true, +this.contratNous, +this.pointsNous, +this.beloteNous);
    }

    euxChanged() {
        this.total(false, +this.contratEux, +this.pointsEux, +this.beloteEux);
    }

    total(nous: boolean, contrat: number, points: number, belote: number) {
        if (!Number.isNaN(points)) {
            let score = Math.trunc(points * 162 / +this.type) + belote;
            let total = 0;
            if (score >= contrat) {
                total = score + contrat;
            }

            if (contrat > 0) {
                if (this.coincher)
                    total *= 2;
                if (this.surcoincher)
                    total *= 2;
            }

            if (nous)
                this.totalNous = total;
            else
                this.totalEux = total;
        }
    }

    addGameTap() {
        this.navCtrl.getPrevious().data.scoreNous = this.totalNous;
        this.navCtrl.getPrevious().data.scoreEux = this.totalEux;
        this.navCtrl.pop();
    }

    bothChanged() {
        this.nousChanged();
        this.euxChanged();
    }
}