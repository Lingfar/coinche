import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AddPage } from '../add/add';
import { Game } from '../../app/models/game';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  private games: Array<Game>;
  private totalNous: number;
  private totalEux: number;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.games = new Array<Game>();
    this.totalNous = 0;
    this.totalEux = 0;
  }

  addTap() {
    this.navCtrl.push(AddPage);
  }

  public ionViewWillEnter() {
    let nous = this.navParams.get('scoreNous');
    let eux = this.navParams.get('scoreEux');

    if (nous !== undefined && eux !== undefined) {
      this.games.push(new Game(nous, eux));
      this.totalNous += nous;
      this.totalEux += eux;
    }
  }
}